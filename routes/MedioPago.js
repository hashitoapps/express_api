module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/medios-pago',
    controllers.MedioPago.GetAll);
  router.get('/medios-pago/:Id',
    controllers.MedioPago.GetById);
  router.post('/medios-pago',
    controllers.MedioPago.Insert);
  router.put('/medios-pago/:Id',
    controllers.MedioPago.Update);
  router.delete('/medios-pago/:Id',
    controllers.MedioPago.Delete);

  return router;
};
