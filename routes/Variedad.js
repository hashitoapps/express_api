module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer el API
   */
  router.get('/variedades',
    controllers.Variedad.GetAll);
  router.get('/variedades/:Id',
    controllers.Variedad.GetById);
  router.post('/variedades',
    controllers.Variedad.Insert);
  router.put('/variedades/:Id',
    controllers.Variedad.Update);
  router.delete('/variedades/:Id',
    controllers.Variedad.Delete);

  return router;
};
