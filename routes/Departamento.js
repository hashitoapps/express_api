module.exports = function(apiVersion) {
  var models      = require('../models');
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
   //llama los departamentos que tienen un idpais determinado
  router.get('/departamentos/:Id', function(req, res) {

    if(req.params.Id==undefined){
      req.params.Id=47;
    }

    models
      .Departamento
      .findAll({
        where: {PaiId: req.params.Id},
        include: [{
          model: models.Municipio
        }]
      })
      .then(function(result) {
        res.send(result);
      })
      .catch(function(error) {
        console.log(error);
        res.status(500).send(error);
      })
  });

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
   //llama los departamentos que tienen un idpais determinado
  router.get('/departamentos/', function(req, res) {

    

    models
      .Departamento
      .findAll({
        where: {PaiId: 47},
        include: [{
          model: models.Municipio
        }]
      })
      .then(function(result) {
        res.send(result);
      })
      .catch(function(error) {
        console.log(error);
        res.status(500).send(error);
      })
  });

  return router;
};
