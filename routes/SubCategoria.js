module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer el API
   */
  router.get('/subcategorias',
    controllers.SubCategoria.GetCategoryByOrder);
  router.get('/subcategorias/:Id',
    controllers.SubCategoria.GetById);
  router.post('/subcategorias',
    controllers.SubCategoria.Insert);
  router.put('/subcategorias/:Id',
    controllers.SubCategoria.Update);
  router.delete('/subcategorias/:Id',
    controllers.SubCategoria.Delete);


  return router;
};
