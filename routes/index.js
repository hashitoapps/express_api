/* Routes Configuration */
module.exports = function(app) {
  /* Declaración de las diferentes rutas que se van a usar en el API */
  app.use('/api/v00',   require('./CategoriaPrincipal')('v00a'));
  app.use('/api/v00',   require('./Direccion')('v00a'));
  app.use('/api/v00',   require('./Imagen')('v00a'));
  app.use('/api/v00',   require('./MedioPago')('v00a'));
  app.use('/api/v00',   require('./Departamento')('v00a'));

  app.use('/api/v00',   require('./Producto')('v00a'));
  app.use('/api/v00',   require('./SubCategoria')('v00a'));
  app.use('/api/v00',   require('./Usuario')('v00a'));
  app.use('/api/v00',   require('./Variedad')('v00a'));
  app.use('/api/v00',   require('./Comentario')('v00a'));
  app.use('/api/v00',   require('./ImagenesBanner')('v00a'));
  app.use('/api/v00',   require('./ProductoMunicipio')('v00a'));
  app.use('/api/v00',   require('./Pais')('v00a'));
};
