module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();

  /**
   * Se asigna cada una de las rutas que va a proveer el API
   */
  router.get('/usuarios',
    controllers.Usuario.GetAll);
  router.get('/usuarios/:Id',
    controllers.Usuario.GetById);
  router.get('/usuarios/:Id/productos',
    controllers.Producto.GetProductosByUserId);

  router.post('/usuarios',
    controllers.Usuario.Insert);
  router.post('/usuarios/login',
    controllers.Usuario.Login);
  router.post('/usuarios/signup',
    controllers.Usuario.Signup);
  router.post('/usuarios/:email/avatar',
    controllers.Usuario.GuardarImagen);
  router.post('/usuarios/facebook',controllers.Usuario.Facebook); 

  router.put('/usuarios/:Id',
    controllers.Usuario.Update);
  router.delete('/usuarios/:Id',
    controllers.Usuario.Delete);

  //criferlo se programa este endpoint para nuevo avatar
  router.post('/usuarios/:Id/nuevoavatar',      
      controllers.Usuario.GuardarAvatar2);

  router.post('/usuarios/recuperarcontra',controllers.Usuario.RecuperarContra);

  router.post('/usuarios/obtenerdes',controllers.Usuario.ObtenerDesencriptado);

  router.post('/usuarios/obtenerporemail',controllers.Usuario.ConsultarPorEmail);

  
  return router;
};
