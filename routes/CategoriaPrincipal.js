module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/categorias',
    controllers.CategoriaPrincipal.GetAll);
  router.get('/categorias/:Id',
    controllers.CategoriaPrincipal.GetById);
  router.post('/categorias',
    controllers.CategoriaPrincipal.Insert);
  router.put('/categorias/:Id',
    controllers.CategoriaPrincipal.Update);
  router.delete('/categorias/:Id',
    controllers.CategoriaPrincipal.Delete);

  return router;
};
