module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer el API
   */
  router.get('/comentarios',
    controllers.Comentario.GetAllComments);
  router.get('/comentarios/:Id',
    controllers.Comentario.GetById);
  router.post('/comentarios',
    controllers.Comentario.Insert);
  router.put('/comentarios/:Id',
    controllers.Comentario.Update);
  router.delete('/comentarios/:Id',
    controllers.Comentario.Delete);
  router.get('/comentarios/:Id/publicacion',
    controllers.Comentario.GetCommentByPublication);
    router.get('/comentarios/:Id/todos',
      controllers.Comentario.GetAllCommentByPublication);

  router.post('/comentarios/:Id/correo-comentario',
    controllers.Comentario.SendCommentMail);

  return router;
};
