module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();

  /**
   * Se asigna cada una de las rutas que va a proveer al API
   */
  router.get('/direcciones',
    controllers.Direccion.GetAll);
  router.get('/direcciones/:Id',
    controllers.Direccion.GetById);
  router.post('/direcciones',
    controllers.Direccion.Insert);
  router.put('/direcciones/:Id',
    controllers.Direccion.Update);
  router.delete('/direcciones/:Id',
    controllers.Direccion.Delete);

  return router;
};
