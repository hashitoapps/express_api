module.exports = function(apiVersion) {
  var controllers = require('../controllers')[apiVersion];
  var express     = require('express');
  var router      = express.Router();
  var multipart   = require('connect-multiparty')();

  /**
   * Se asigna cada una de las rutas que va a proveer el API
   */
  router.get('/productos',
    controllers.Producto.GetAll);
  router.get('/productos/filter',
    controllers.Producto.GetProductPageFilter);
  router.get('/productos/:Id',
    controllers.Producto.GetById);
  router.get('/productos/:Id/visitar',
    controllers.Producto.GetProductAddVisit);
  router.get('/productos/:Id/similares',
    controllers.Producto.LoadSimilar);
  router.get('/productos/:DescripcionCorta',
      controllers.Producto.GetProductsByWord);
  router.get('/productos/:Estado/estado',
      controllers.Producto.GetProductsByState);

  router.post('/productos',
    controllers.Producto.Insert);
  router.post('/productos/:Id/imagen',
    multipart,
    controllers.Producto.CargarImagen);
  router.post('/productos/:Id/correo-contacto',
    controllers.Producto.SendContactMail);
  router.post('/productos/:Id/correo_aprobar',
    controllers.Producto.SendApprovePublication);
  router.post('/productos/correo_respuesta',
    controllers.Producto.SendResultApproveMail);
  router.post('/productos/correo-contactenos',
    controllers.Producto.SendContactenosMail);

  router.put('/productos/:Id',
    controllers.Producto.Update);
  router.delete('/productos/:Id',
    controllers.Producto.Delete);

  //nuevo
  router.post('/productos/:Id/imagen-base64',
    controllers.Producto.UploadImage64);

  return router;
};
