var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.SubCategoria, [
  {
    model: models.Variedad,

  }

]);

repository.Validate = function(model) {
  var error = null;

  if (model.Nombre === undefined ||
      model.Nombre === null) {
    error = 'Debe de proporcionar un nombre de sub-cateogría valido';
  }

  return error;
};

module.exports = repository;
