var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Comentario);

repository.GetCommentByPublication = function(IdPublicacion) {
 
  return new Promise(function(resolve, reject) {
     console.log("Entro aqui......",models.Comentario);
    models.Comentario
      .find({
        where: {IdPublicacion: IdPublicacion}
      })
      .then(function(model) {
        return resolve(model);
        
      })
      .catch(function(error) {
        return reject(error);
      })
  });
}

repository.Validate = function(model) {
  var error = null;

  if (model.Comentario === undefined || model.Comentario === null) {
    error = 'Debe de ingresar un comentario';
  }
  if (model.IdPublicacion === undefined || model.IdPublicacion === null) {
    error = 'No existe esta publicacion';
  }

  return error;
}

module.exports = repository;
