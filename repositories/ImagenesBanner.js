var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.ImagenesBanner, []);

repository.Validate = function(model) {
  var error = null;

  if (model.Ruta === undefined || model.Ruta === null) {
    error = 'Debe de ingresar una ruta valida';
  }

  return error;
}

module.exports = repository;
