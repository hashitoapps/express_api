var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Usuario, [models.Pais]);

repository.Validate = function(model) {
  var error = null;

  if (model.Correo === undefined || model.Correo === null) {
    error = 'Debe de ingresar un correo válido';
  }
  if (model.Nombre === undefined || model.Nombre === null) {
    error = 'Debe de ingresar un nombre Válido';
  }
  if (model.Clave === undefined || model.Clave === null) {
    error = 'Debe de ingresar una clave válida';
  }

  return error;
}

repository.LoadByEmail = function(email){
  return models.Usuario.find({
      where: {
        Correo: email
      },
      include: [models.Pais]
    });

}

module.exports = repository;
