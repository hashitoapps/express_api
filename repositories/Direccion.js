var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var repository = Repository(models.Direccion, []);

repository.Validate = function(model) {
  var error = null;

  if (model.Linea1 === undefined || model.Linea1 === null) {
    error = 'Debe de ingresar una dirección válida';
  }
  if (model.Linea2 === undefined || model.Linea2 === null) {
    error = 'Debe de ingresar una dirección válida';
  }
  if (model.Referencia === undefined || model.Referencia === null) {
    error = 'Debe de ingresar una referencia válida';
  }
  if (model.Latitud === undefined || model.Latitud === null) {
    error = 'Debe de ingresar una latitud válida';
  }
  if (model.Longitud === undefined || model.Longitud === null) {
    error = 'Debe de ingresar una longitud válida';
  }

  return error;
}

module.exports = repository;
