var models     = require('../models');
var Promise    = require('bluebird');
var Repository = require('./Repository');
var includes   = [{
    model: models.Producto
  }, {
    model: models.Municipio
  }];
var repository = Repository(models.ProductoMunicipio, includes);


repository.DeleteAll = function(ProductoId){
	return models.ProductoMunicipio.destroy({
        where:{ ProductoId: ProductoId}
      });
}

module.exports = repository;
