"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  Departamento
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {

	var Pais = sequelize.define('Pais', {
		Id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},		
		Nombre: { type: DataTypes.STRING, allowNull: false},
		Codigo: { type: DataTypes.STRING, allowNull: false},
	}, {
		classMethods: {
			associate: function (models) {
				Pais.hasMany(models.Departamento);
				Pais.hasMany(models.Usuario);
			}
		}
	});

	return Pais;
};