"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  Comentarioss
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {
	var Comentarios = sequelize.define('Comentario', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		Comentario: { type: DataTypes.STRING, allowNull: false},
		IdPublicacion: { type: DataTypes.INTEGER, allowNull: false}
	}, {
		classMethods: {
			associate: function (models) {
				Comentarios.belongsTo(models.Producto, {foreignKey: 'Id'});
			}
		}
	});

	return Comentarios;
};
