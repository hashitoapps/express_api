"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  Municipio
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {

	var ProductoMunicipio = sequelize.define('ProductoMunicipio', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
	}, {
		classMethods: {
			associate: function (models) {
				ProductoMunicipio.belongsTo(models.Producto);
				ProductoMunicipio.belongsTo(models.Municipio);
			}
		}
	});

	return ProductoMunicipio;
};
