"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  MedioPago
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {

	var MedioPago = sequelize.define('MedioPago', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},		
		Nombre : { type: DataTypes.STRING, allowNull: false},
	}, {
		classMethods: {
			associate: function (models) {
				MedioPago.belongsToMany(models.SubCategoria, {through: 'MedioPagoSubCategoria'});
				MedioPago.hasMany(models.Pedido);	
			}
		}
	});

	return MedioPago;
};