"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  Restaurante
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {

	var Direccion = sequelize.define('Direccion', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		Linea1: {type:DataTypes.STRING, allowNull: true},
		Linea2: {type:DataTypes.STRING, allowNull: true},			
		Referencia: {type:DataTypes.STRING, allowNull: true},	
		Latitud: {type:DataTypes.FLOAT, allowNull: true},
		Longitud: {type:DataTypes.FLOAT, allowNull: true},		
	}, {
		classMethods: {
			associate: function (models) {
				Direccion.belongsTo(models.Usuario);
			}
		}
	});

	return Direccion;
};