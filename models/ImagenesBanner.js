"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  Comentarioss
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {
	var ImagenesBanner = sequelize.define('ImagenesBanner', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		Ruta: { type: DataTypes.STRING, allowNull: false},
		Orden: { type: DataTypes.INTEGER,allowNull: true}
	 }
	);

	return ImagenesBanner;
};
