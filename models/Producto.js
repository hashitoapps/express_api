'use strict';

//-------------------------------------------------------------------------
// ENTIDAD  Producto
//-------------------------------------------------------------------------

module.exports = function(sequelize, DataTypes) {
  var Producto = sequelize.define('Producto', {
    Id : {type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
    Nombre: {type: DataTypes.STRING, allowNull: false},
    DescripcionCorta: {type: DataTypes.STRING, allowNull: false},
    DescripcionLarga: {type: DataTypes.TEXT, allowNull: false},
    Precio: {type: DataTypes.DECIMAL(10, 2), allowNull: false},
    PrecioDesc: {type: DataTypes.DECIMAL(10, 2), allowNull: false},
    PorcentajePromo: {type: DataTypes.DECIMAL(4, 2), allowNull: false},
    ImagenPrincipal: {type: DataTypes.STRING, allowNull: false},
    Visitas: {type: DataTypes.INTEGER, allowNull: false},
    MeGusta: {type: DataTypes.INTEGER, allowNull: true},
    NoMeGusta: {type: DataTypes.TEXT, allowNull: true},
    Estado: {type: DataTypes.TEXT, allowNull: true},
    //P Producto A Adicional
    TipoProducto: {type: DataTypes.ENUM('P', 'A'), allowNull: false},
  }, {
    classMethods: {
      associate: function(models) {
        Producto.belongsTo(models.Variedad);
        Producto.belongsTo(models.Municipio);
        Producto.belongsTo(models.Producto);
        Producto.belongsTo(models.Usuario);
        Producto.hasMany(models.Imagen);
        Producto.hasMany(models.Producto);
        Producto.hasMany(models.ProductoMunicipio);
      }
    }
  });

  return Producto;
};
