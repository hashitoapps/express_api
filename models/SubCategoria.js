"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  SubCategoria
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {
	var SubCategoria = sequelize.define('SubCategoria', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},
		Nombre: { type: DataTypes.STRING, allowNull: false},
		Orden: { type: DataTypes.STRING, allowNull: false}
	}, {
		classMethods: {
			associate: function (models) {
				SubCategoria.belongsTo(models.CategoriaPrincipal);
				SubCategoria.hasMany(models.Variedad);
			}
		}
	});

	return SubCategoria;
};
