"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  Restaurante
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {

	var DetallePedido = sequelize.define('DetallePedido', {
		Id : { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},		
		Cantidad: { type: DataTypes.INTEGER, allowNull: false},
		PorcentajePromo: { type: DataTypes.DECIMAL(4,2), allowNull: false},
		Precio: { type: DataTypes.DECIMAL(10,2), allowNull: false},
	}, {
		classMethods: {
			associate: function (models) {
				DetallePedido.belongsTo(models.Pedido);
				DetallePedido.belongsTo(models.Producto);
				DetallePedido.belongsTo(models.DetallePedido);
				DetallePedido.hasMany(models.DetallePedido);
			}
		}
	});

	return DetallePedido;
};