"use strict";

//-------------------------------------------------------------------------
// ENTIDAD  Departamento
//-------------------------------------------------------------------------

module.exports = function (sequelize, DataTypes) {

	var Departamento = sequelize.define('Departamento', {
		Id: { type: DataTypes.INTEGER, primaryKey : true, autoIncrement: true},		
		Nombre: { type: DataTypes.STRING, allowNull: false},
		Codigo: { type: DataTypes.STRING, allowNull: false},
	}, {
		classMethods: {
			associate: function (models) {
				Departamento.belongsTo(models.Pais);
				Departamento.hasMany(models.Municipio);
			}
		}
	});

	return Departamento;
};