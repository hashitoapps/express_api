var models        = require('../../models');
var repository    = require('../../repositories/Producto');
var repositoryImg = require('../../repositories/Imagen');
var controller    = require('./BaseController')(repository);
var nodemailer    = require('nodemailer');
var Promise       = require('bluebird');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'carlosmaroz@gmail.com', // my mail
        pass: 'CMortiz123'
    }
}));
const Sequelize = require('sequelize');

controller.GetProductAddVisit = function(req, res) {
  var params = req.params;

  console.log(req._startTime);
  console.log(req._remoteAddress);
  console.log(req.headers.origin + req.url);

  repository
    .GetProductAddVisit(params.Id)
    .then(function(model) {
      res.send(model);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};

controller.GetProductPageFilter = function(req, res) {
  console.log("******* Aqui Inicia *************");
  var params    = req.params;
  var page      = req.query.page      || 1;
  var pageSize  = req.query.pageSize  || 100;
  var orderBy   = req.query.orderBy   || 'Nombre';
  var sortOrder = req.query.sortOrder || 'ASC';
  var promise   = new Promise(function(resolve, reject) {
    return resolve({});
  });

  if (req.query.IdVariedad) {
    console.log("Variedades ",req.query.IdVariedad);
    
    promise = new Promise(function(resolve, reject) {
      return resolve({VariedadId: req.query.IdVariedad});
    }).catch(function(error) {
          return reject(error);
    })
  } else if (req.query.IdSubCategoria) {
    console.log("Entro aqui. categoria");

    promise = new Promise(function(resolve, reject) {
      models
        .SubCategoria
        .find({
          where: {Id: req.query.IdSubCategoria},
          include: [{model: models.Variedad}]
        })
        .then(function(models) {
          var results = [];

          [].forEach.call(models.Variedads, function(item) {
            results.push(item.Id);
          });

          return resolve({VariedadId: results});

        })
        .catch(function(error) {
          return reject(error);
        })
    });
  } else if (req.query.IdCategoria) {
    promise = new Promise(function(resolve, reject) {
      models
        .CategoriaPrincipal
        .find({
          where: {Id: req.query.IdCategoria},
          include: [
            {
              model: models.SubCategoria,
              include: [
                {
                  model: models.Variedad
                }
              ]
            }
          ]
        })
        .then(function(models) {
          var results = [];
          [].forEach.call(models.SubCategoria, function(item) {
            [].forEach.call(item.Variedads, function(vari) {
              results.push(item.Id);
            });
          });
         
          return resolve({VariedadId: results});
        })
        .catch(function(error) {
          return reject(error);
        })
    });

  }

  promise
    .then(function(query) {
      console.log("4********",req.query);
      if (req.query.MinValue) {
        query.Precio = {};
        query.Precio.$gte = req.query.MinValue;
      }

      if (req.query.MaxValue) {
        query.Precio = query.Precio || {};
        query.Precio.$lte = req.query.MaxValue;
      }

      query.Estado=1;

      return repository
        .LoadPageByQuery2(query, page, pageSize, orderBy, sortOrder,req.query.IdMunicipio,req.query.Descripcion)
    })
    .then(function(models) {
      res.send(models);
    }) .catch(function(error) {
          return res.status(500).send(error);
        });
};

controller.GetProductosByUserId = function(req, res) {
  var params    = req.params;
  var userId    = params.Id;
  var page      = req.query.page      || 1;
  var pageSize  = req.query.pageSize  || 100;
  var orderBy   = req.query.orderBy   || 'Nombre';
  var sortOrder = req.query.sortOrder || 'ASC';
  var promise   = new Promise(function(resolve, reject) {
    return resolve({});
  });

  if (req.query.IdVariedad) {
    promise = new Promise(function(resolve, reject) {
      return resolve({VariedadId: req.query.IdVariedad});
    });
  } else if (req.query.IdSubCategoria) {
    promise = new Promise(function(resolve, reject) {
      models
        .SubCategoria
        .find({
          where: {Id: req.query.IdSubCategoria},
          include: [{model: models.Variedad}]
        })
        .then(function(models) {
          var results = [];

          [].forEach.call(models.Variedads, function(item) {
            results.push(item.Id);
          });

          return resolve({VariedadId: results});
        })
        .catch(function(error) {
          return reject(error);
        })
    });
  } else if (req.query.IdCategoria) {
    promise = new Promise(function(resolve, reject) {
      models
        .CategoriaPrincipal
        .find({
          where: {Id: req.query.IdCategoria},
          include: [
            {
              model: models.SubCategoria,
              include: [
                {
                  model: models.Variedad
                }
              ]
            }
          ]
        })
        .then(function(models) {
          var results = [];

          [].forEach.call(models.SubCategoria, function(item) {
            [].forEach.call(item.Variedads, function(vari) {
              results.push(vari.Id);
            });
          });

          return resolve({VariedadId: results});
        })
        .catch(function(error) {
          return reject(error);
        })
    });
  }

  promise
    .then(function(query) {
      console.log("Entro para consultar los productos");
      query.UsuarioId = userId;

      if (req.query.MinValue) {
        query.Precio = {};
        query.Precio.$gte = req.query.MinValue;
      }

      if (req.query.MaxValue) {
        query.Precio = query.Precio || {};
        query.Precio.$lte = req.query.MaxValue;
      }
      query.Estado=1;
      console.log(query);

      return repository
        .LoadPageByQuery(query, page, pageSize, orderBy, sortOrder)
    })
    .then(function(models) {
      res.send(models);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};

controller.LoadSimilar = function(req, res) {
  var params   = req.params;
  var pageSize = 3;

  repository
    .LoadSimilar(params.Id, pageSize)
    .then(function(models) {
      res.send(models);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};

controller.SendContactMail = function(req, res) {
  var body   = req.body;
  var params = req.params;

  console.log(body);

  repository
    .LoadById(params.Id)
    .then(function(result) {
      var mailOptions = {
        from: 'Grupo Prevenir Express <clientes@prevenirexpress.com>',
        to: result.Usuario.Correo,
        subject: ['Contacto', result.Nombre].join(' ')
      };

      //mailOptions.text = body.Message;
      mailOptions.html = "<h1><i> Mensaje desde: Grupo Prevenir Express</i></h1><h2>El usuario</h2>"+body.Name+
      " <h2>Correo:</h2>"+body.Email+" <h2>Tiene la siguiente inquietud:</h2>"+body.Message;

      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }

        res.status(200).send(info.response);
      });

      //copia de seguridad
      var correo_prevenir='clientesprevenir@outlook.es';
      var mailOptions_ = {
        from: 'Grupo Prevenir Express <clientes@prevenirexpress.com>',
        to: correo_prevenir,
        subject: ['Contacto', result.Nombre].join(' ')
      };

      //mailOptions.text = body.Message;
      mailOptions_.html = "<h1><i> Mensaje desde: Grupo Prevenir Express</i></h1><h2>El usuario</h2>"+body.Name+
      " <h2>Correo:</h2>"+body.Email+" <h2>Tiene la siguiente inquietud:</h2>"+body.Message;

      transporter.sendMail(mailOptions_, function(error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }

        res.status(200).send(info.response);
      });
      //fin copia de seguridad


    })
    .catch(function(error) {
      console.log(error);
      res.status(500).send(error);
    });
};

controller.SendContactenosMail = function(req, res) {
  var body   = req.body;
  var params = req.params;

  console.log(body);

      var mailOptions = {
        from: 'Grupo Prevenir Express <clientes@prevenirexpress.com>',
        to: 'prevenirexpress@gmail.com',
        subject: ['Contacto', body.Name].join(' ')
      }

      //mailOptions.text = body.Message;
      mailOptions.html = "<h1><i> Mensaje desde: Grupo Prevenir Express</i></h1><h2>El usuario</h2>"+body.Name+
      " <h2>Correo:</h2>"+body.Email+" <h2>Tiene la siguiente inquietud:</h2>"+body.Message;

      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }
        res.status(200).send(info.response);
      });
};

controller.SendApprovePublication = function(req, res) {
  var body   = req.body;
  var params = req.params;

  console.log(body);

      var mailOptions = {
        from: 'Grupo Prevenir Express <clientes@prevenirexpress.com>',
        to: 'prevenirexpress@gmail.com',
        subject: ['Aprobar publicación'].join(' ')
      }

      //mailOptions.text = body.Message;
      mailOptions.html = "<h1><i> Mensaje desde: Grupo Prevenir Express</i></h1><h2>El usuario</h2>"+body.Nombre+
      " <h2>Correo:</h2>"+body.Correo+" <h2>Ha realizado la siguiente publicacion:</h2>"+body.Url;

      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Message sent: ' + info.response);
        }
        res.status(200).send(info.response);
      });
};

controller.SendResultApproveMail = function(req, res) {
  var body   = req.body;
  var params = req.params;

console.log(body);

      var mailOptions = {
        from: 'Grupo Prevenir Express <clientes@prevenirexpress.com>',
        to: body.Correo,
        subject: 'Comentario desde PrevenirExpress'
      };

      //mailOptions.text = body.Message;
      mailOptions.html = "<h1><i> Mensaje de Grupo Prevenir Expres: </i></h1><h2>Se ha dado respuesta a su publicacion: </h2>"+body.Razon+" debido a que: "+body.Comentario;

      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          console.log("Error enviando mensaje: "+error);
        } else {
          console.log('Message sent: ' + info.response);
        }

        res.status(200).send(info.response);
      });
};

controller.CargarImagen = function(req, res) {
  var fs          = require('fs');
  var file        = req.files.file;
  var producto    = req.params.Id;
  var nPath       = 'public/productos/' + producto + '/';
  var extensiones = [
    {ctype: 'image/jpeg',  ext: 'jpg'},
    {ctype: 'image/pjpeg', ext: 'jpeg'},
    {ctype: 'image/png',   ext: 'png'},
    {ctype: 'image/bmp',   ext: 'bmp'}
  ];

  var extension = extensiones.find(function(ext) {
    return ext.ctype == file.headers['content-type'];
  });

  if (extension) {
    var path    = file.path;
    var newPath = [
      nPath,
      generateUUID(),
      '.',
      extension.ext].join('');

    fs.mkdir(nPath, function(e) {
      var is      = fs.createReadStream(path);
      var os      = fs.createWriteStream(newPath);

      is.pipe(os);

      is.on('end', function() {
        try {
          //eliminamos el archivo temporal
          fs.unlinkSync(path);

          newPath = [
            [req.protocol, req.hostname].join('://'),
            newPath.replace('public/', '')
          ].join('/');

          console.log(newPath);

          repositoryImg
            .Insert({
              RutaGrande: newPath,
              RutaPequeño: newPath,
              ProductoId: producto
            })
            .then(function(result) {
              res.send(result);
            })
            .catch(function(error) {
              console.log(error);
              res.status(500).send(error);
            })


        } catch (err) {
          res.status(500).send('error al cargar archivo ' + err);
        }
      });
    });
  } else {
    res.status(500).send([
      'Formato de archivo incorrecto,',
      'debe de cargar una imagen'
    ].join(' '));
  }
};

controller.GetProductsByState = function(req, res){
  var page      = req.query.page      || 1;
  var pageSize  = req.query.pageSize  || 100;
  var orderBy   = req.query.orderBy   || 'Nombre';
  var sortOrder = req.query.sortOrder || 'ASC';

  promise = new Promise(function(resolve, reject) {
    repository
      .GetProductsByState(1)
      .then(function(model) {
        res.send(model);
      })
      .catch(function(error) {
        res.status(500).send(error);
      });
  });
}

controller.GetProductsByWord = function(req, res) {
  var page      = req.query.page      || 1;
  var pageSize  = req.query.pageSize  || 100;
  var orderBy   = req.query.orderBy   || 'DescripcionCorta';
  var sortOrder = req.query.sortOrder || 'ASC';
  var promise   = new Promise(function(resolve, reject) {
    return resolve({});
  });

  promise = new Promise(function(resolve, reject) {
    repository
      .find({
        where: {DescripcionCorta: req.query.DescripcionCorta,
        Estado:1}
      })
      .then(function(models) {
        return repository
          .LoadPageByQuery(query, page, pageSize, orderBy, sortOrder)
      })
      .catch(function(error) {
        return reject(error);
      })
  });
};

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

//cambio 29 Julio
controller.UploadImage64 = function(req, res) {

  //console.log(req.body);
  //console.log(req.rawBody);

  //var base64Data = req.body.imagen.replace(/^data:image\/png;base64,/, "");
  var base64Data = req.body.imagen.replace("data:image\/*;charset=utf-8;base64,", "");
  //var base64Data = req.body.imagen;

  console.log("----------------------------hola");
  console.log(base64Data);

   
  var fs          = require('fs');

  //var file        = req.files.file;
  var producto    = req.params.Id;
  var nPath       = 'public/productos/' + producto + '/';
  if (!fs.existsSync(nPath)) {
    // Do something
    fs.mkdirSync(nPath);
  }

  var extensiones = [
    {ctype: 'image/jpeg',  ext: 'jpg'},
    {ctype: 'image/pjpeg', ext: 'jpeg'},
    {ctype: 'image/png',   ext: 'png'},
    {ctype: 'image/bmp',   ext: 'bmp'}
  ];

  //var extension = extensiones.find(function(ext) {
  //  return ext.ctype == file.headers['content-type'];
  //});

  //if (extension) {
    //var path    = file.path;
    var newPath = [
      nPath,
      generateUUID(),
      '.jpg',
      ].join('');

    require("fs").writeFile(newPath, base64Data, 'base64', function(err) {
      console.log(err);
    });

    newPath = [
            [req.protocol, req.hostname].join('://'),
            newPath.replace('public/', '')
          ].join('/');


    repositoryImg
        .Insert({
          RutaGrande: newPath,
          RutaPequeño: newPath,
          ProductoId: producto
        })
        .then(function(result) {
          res.send(result);
        })
        .catch(function(error) {
          console.log(error);
          res.status(500).send(error);
        })
};


module.exports = controller;
