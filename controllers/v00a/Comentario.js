var models        = require('../../models');
var repository    = require('../../repositories/Comentario');
var controller    = require('./BaseController')(repository);
var nodemailer    = require('nodemailer');
var Promise       = require('bluebird');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'carlosmaroz@gmail.com', // my mail
        pass: 'CMortiz123'
    }
}));

controller.GetAllComments= function(req, res) {
  var page      = req.query.page      || 1;
  var pageSize  = req.query.pageSize  || 100;
  var orderBy   = req.query.orderBy   || 'Id';
  var sortOrder = req.query.sortOrder || 'ASC';
  repository
    .LoadPage(page, pageSize, orderBy, sortOrder)
    .then(function(models) {
      res.send(models);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
}

controller.GetAllCommentByPublication = function(req, res) {
   
  var params    = req.params;
  var page      = req.query.page      || 1;
  var pageSize  = req.query.pageSize  || 100;
  var orderBy   = req.query.orderBy   || 'IdPublicacion';
  var sortOrder = req.query.sortOrder || 'ASC';
   var promise   = new Promise(function(resolve, reject) {
    return resolve({});
  });
 
  promise
    .then(function(query) {

      query = {IdPublicacion: params.Id}
       return repository
          .LoadPageByQuery(query, page, pageSize, orderBy, sortOrder)
  }).then(function(models) {
      res.send(models);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
}

controller.GetCommentByPublication = function(req, res) {

  var params    = req.params;
  repository
    .GetCommentByPublication(params.Id)
    .then(function(model) {
      res.send(model);
      //console.log("Entro aquixxxxxxxxxxxxxx",res);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};

controller.SendCommentMail = function(req, res) {
  var body   = req.body;
  var params = req.params;

  console.log("este es req.id:"+params.Id);

  repository
    .LoadById(params.Id)
    .then(function(result) {
      console.log(result);
      var mailOptions = {
        from: 'Grupo Prevenir Express <clientes@prevenirexpress.com>',
        to: body.Correo,
        subject: 'Comentario desde PrevenirExpress'
      };

      //mailOptions.text = body.Message;
      mailOptions.html = "<h1><i> Mensaje de: "+body.Origen+" </i></h1><h2>Se ha enviado el siguiente comentario:</h2>"+body.Comentario;

      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          console.log("Error enviando mensaje: "+error);
        } else {
          console.log('Message sent: ' + info.response);
        }

        res.status(200).send(info.response);
      });
    })
    .catch(function(error) {
      console.log(error);
      res.status(500).send(error);
    });
};


module.exports = controller;
