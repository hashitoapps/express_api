var models        = require('../../models');
var repository    = require('../../repositories/ProductoMunicipio');
var controller    = require('./BaseController')(repository);
var nodemailer    = require('nodemailer');
var Promise       = require('bluebird');

controller.DeleteAll = function(req,res){	
	repository.DeleteAll(req.params.Id);
  var result = {
    "result":"ok"
  }
	res.status(200).send(result);
}

controller.GetByProduct = function(req,res){
	models.ProductoMunicipio
      .findAndCountAll({
        where: {ProductoId: req.params.Id},
        include: [{
		    model: models.Producto
		  }, {
		    model: models.Municipio
		  }]
      })
      .then(function(models) {
        return res.send(models);
      })
      .catch(function(error) {
        return reject(error);
      })
  
}

module.exports = controller;
