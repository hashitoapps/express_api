var repository = require('../../repositories/SubCategoria');
var controller = require('./BaseController')(repository);

module.exports = controller;

controller.GetCategoryByOrder = function(req, res) {
  var page      = req.query.page      || 1;
  var pageSize  = req.query.pageSize  || 100;
  var orderBy   = req.query.orderBy   || 'Orden';
  var sortOrder = req.query.sortOrder || 'ASC';

  repository
    .LoadPage(page, pageSize, orderBy, sortOrder)
    .then(function(models) {
      res.send(models);
    })
    .catch(function(error) {
      res.status(500).send(error);
    });
};
