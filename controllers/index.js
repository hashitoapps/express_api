var controllers = [];

controllers['v00'] = {
  User: require('./v00/user'),
};

controllers['v00a'] = {
  CategoriaPrincipal: require('./v00a/CategoriaPrincipal'),
  Direccion: require('./v00a/Direccion'),
  Imagen: require('./v00a/Imagen'),
  MedioPago: require('./v00a/MedioPago'),
  Producto: require('./v00a/Producto'),
  SubCategoria: require('./v00a/SubCategoria'),
  Usuario: require('./v00a/Usuario'),
  Variedad: require('./v00a/Variedad'),
  Comentario: require('./v00a/Comentario'),
  ImagenesBanner: require('./v00a/ImagenesBanner'),
  ProductoMunicipio: require('./v00a/ProductoMunicipio'),
};

module.exports = controllers;
